package com.example.shop.repository;

import com.example.shop.domain.Product;
import com.example.shop.enums.ProductStatusEnum;
import com.example.shop.enums.ProductTypeEnum;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@EnableJpaAuditing
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void findOne() throws Exception {
        Product product = productRepository.findOne("r4DJ9P73HGOnP_7vaCXVn0A01");
        System.out.println(product);
    }

    @Test
    public void save(){
        Product product = new Product();
        product.setName("洗衣服2");
        product.setOld_price(998l);
        product.setPrice(9l);
        product.setLogo("logo");
        product.setImage("image");
        product.setHeading("限时折扣");
        product.setDescription("干净");
        product.setStock(10000);
        product.setDate(System.currentTimeMillis());
        product.setSort(0);
        product.setType(ProductTypeEnum.SERVICE.getCode());
        product.setStatus(ProductStatusEnum.UP.getCode());
        Product result = productRepository.save(product);
        Assert.assertNotNull(result);


    }

}