package com.example.shop.service.impl;

import com.example.shop.domain.Order;
import com.example.shop.service.OrderService;
import com.example.shop.service.PayService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest
@RunWith(SpringRunner.class)
public class PayServiceImplTest {

    @Autowired
    private PayService payService;

    @Autowired
    private OrderService orderService;
    @Test
    public void create() throws Exception {
        Order order =orderService.findOne("0vct-7noE06RXyIYTTge-3A02");
        payService.create(order);

    }
}