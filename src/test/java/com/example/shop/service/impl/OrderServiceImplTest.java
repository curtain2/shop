package com.example.shop.service.impl;

import com.example.shop.domain.Order;
import com.example.shop.enums.OrderStatusEnum;
import com.example.shop.repository.OrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class OrderServiceImplTest {

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepository;

    @Before
    public void init(){
        Order order = new Order();
        order.setOrderStatus(OrderStatusEnum.NOT_PAY_STATUS.getCode());
        order.setAddress("北京市 市辖区 东城区加加加");
        order.setName("zzk");
        order.setAmount((long) 5_000);
        order.setNumber("111111");

        Order order1 = new Order();
        order1.setOrderStatus(OrderStatusEnum.NOT_PAY_STATUS.getCode());
        Order order2 = new Order();
        order2.setOrderStatus(OrderStatusEnum.NOT_PAY_STATUS.getCode());
        Order order3 = new Order();
        order3.setOrderStatus(OrderStatusEnum.NEW.getCode());
        Order order4 = new Order();
        order4.setOrderStatus(OrderStatusEnum.FINISHED.getCode());
        Order order5 = new Order();
        order5.setOrderStatus(OrderStatusEnum.CANCEL.getCode());
        Order order6 = new Order();
        order6.setOrderStatus(OrderStatusEnum.NOT_PAY_STATUS.getCode());
        Order order7 = new Order();
        order7.setOrderStatus(OrderStatusEnum.EXPORT.getCode());

        List<Order> orders = new ArrayList<>();
        orders.add(order);
        orders.add(order1);
        orders.add(order2);
        orders.add(order3);
        orders.add(order4);
        orders.add(order5);
        orders.add(order6);
        orders.add(order7);

        when(orderRepository.findByNumber(order.getNumber())).thenReturn(order);
        when(orderRepository.findByCreatetimeBetween(any(), any())).thenReturn(orders);
    }

    @Test
    public void getOrderByOrderNumber(){
        String number = "111111";
        Order order = orderService.findByOrderNumber(number);
        assertThat(order).hasFieldOrPropertyWithValue("number", number);
    }

    @Test
    public void statisticsOrder(){
        Map<Integer, Long> map = orderService.statisticsOrder(0, 1000);
        System.out.println(map);
    }
}