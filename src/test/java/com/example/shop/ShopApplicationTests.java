package com.example.shop;

import com.example.shop.domain.*;
import com.example.shop.enums.OrderStatusEnum;
import com.example.shop.enums.PayStatusEnum;
import com.example.shop.enums.ProductStatusEnum;
import com.example.shop.enums.ProductTypeEnum;
import com.example.shop.repository.OrderRepository;
import com.example.shop.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShopApplicationTests {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private OrderRepository orderRepository;


	@Test
	public void test(){
		double v = Double.valueOf("19.9")* 100;
		int fee = Double.valueOf(v).intValue();
		System.out.println(fee);

		BigDecimal v1 = new BigDecimal("19.9");
		BigDecimal v2 = new BigDecimal("100");
		Double b = v1.multiply(v2).doubleValue();
		int fee1 = b.intValue();
		System.out.println(fee1);
		System.out.println(v1.doubleValue());
	}

	@Test
	public void contextLoads() {


		Product product = new Product();
		product.setName("书包2");
		product.setOld_price(998l);
		product.setPrice(98l);
		product.setLogo("logo");
		product.setImage("image");
		product.setHeading("限时折扣");
		product.setDescription("好用");
		product.setStock(10000);
		product.setDate(System.currentTimeMillis());
		product.setSort(0);
		product.setType(ProductTypeEnum.PRACTICALITY.getCode());
		product.setStatus(ProductStatusEnum.UP.getCode());
		Product result = productRepository.save(product);

		Product product2 = new Product();
		product2.setName("洗衣服2");
		product2.setOld_price(998l);
		product2.setPrice(98l);
		product2.setLogo("logo");
		product2.setImage("image");
		product2.setHeading("限时折扣");
		product2.setDescription("好用");
		product2.setStock(10000);
		product2.setDate(System.currentTimeMillis());
		product2.setSort(0);
		product2.setType(ProductTypeEnum.SERVICE.getCode());
		product2.setStatus(ProductStatusEnum.UP.getCode());
		Product result2 = productRepository.save(product2);


		OrderItem orderItem = new OrderItem();
		orderItem.setCount(10);
		orderItem.setProduct(result);

		OrderItem orderItem2 = new OrderItem();
		orderItem2.setCount(10);
		orderItem2.setProduct(result2);

		Order order = new Order();
		order.setPayStatus(PayStatusEnum.SUCCESS.getCode());
		order.setOrderStatus(OrderStatusEnum.FINISHED.getCode());
		order.getItems().add(orderItem);
		order.getItems().add(orderItem2);
		orderRepository.save(order);
	}

}
