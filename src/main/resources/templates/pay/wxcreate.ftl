<script>
    function onBridgeReady(){
        WeixinJSBridge.invoke(
                'getBrandWCPayRequest',
                {
                    "appId":"${wxPayMpOrderResult.appId}",     //公众号名称，由商户传入
                    "timeStamp":"${wxPayMpOrderResult.timeStamp}",         //时间戳，自1970年以来的秒数
                    "nonceStr":"${wxPayMpOrderResult.nonceStr}", //随机串
                    "package":"${wxPayMpOrderResult.packageValue}",
                    "signType":"MD5",         //微信签名方式：
                    "paySign":"${wxPayMpOrderResult.paySign}" //微信签名
                },
                function(res){
                    document.writeln(JSON.stringify(res))
                    document.writeln("APPID+${wxPayMpOrderResult.appId}")
                    document.writeln("TIME+${wxPayMpOrderResult.timeStamp}")
                    document.writeln("NONCE+${wxPayMpOrderResult.nonceStr}")
                    document.writeln("PACKAGE+${wxPayMpOrderResult.packageValue}")
                    document.writeln("SIGN+${wxPayMpOrderResult.paySign}")

                    if(res.err_msg == "get_brand_wcpay_request:ok" ) {
                    }     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。
                }
        );
    }
    if (typeof WeixinJSBridge == "undefined"){
        if( document.addEventListener ){
            document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
        }else if (document.attachEvent){
            document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
            document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
        }
    }else{
        onBridgeReady();
    }

    function delayURL(url,time) {
        setTimeout()

    }


</script>

