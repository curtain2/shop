var wxpay=function (data){
    var errmsg="";
    function onBridgeReady(){
        WeixinJSBridge.invoke(
            'getBrandWCPayRequest',data
            // {
            //     "appId":"wx3c6699da59f7cb25",     //公众号名称，由商户传入
            //     "timeStamp":"1513680571",         //时间戳，自1970年以来的秒数
            //     "nonceStr":"jmlFDdzNRFDZZx7q", //随机串
            //     "package":"prepay_id=wx20171219184931dc15c24bce0024388774",
            //     "signType":"MD5",         //微信签名方式：
            //     "paySign":"00F9BE3C2C9E1F4057FB3B946F965C6C" //微信签名
            // }
            ,
            function(res){
                document.write(JSON.stringify(res));
                document.write(JSON.stringify(data));
                if(res.err_msg == "get_brand_wcpay_request:ok" ) {
                    errmsg="ok";
                }    
                else if(res.err_msg=="get_brand_wcpay_request:cancel"){
                    errmsg="cancel";
                }else if(res.err_msg=="get_brand_wcpay_request:fail"){
                    errmsg="fail";
                } 
            }
        );
    }
    if (typeof WeixinJSBridge == "undefined"){
        if( document.addEventListener ){
            document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
        }else if (document.attachEvent){
            document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
            document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
        }
    }else{
        onBridgeReady();
    }
    return errmsg;
}