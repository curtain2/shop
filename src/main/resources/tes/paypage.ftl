<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_493372_i1o64cmgub3uwhfr.css" type="text/css" />
    <title>Document</title>
</head>

<body>
    <div style="display:inline;" id="paypage">
        <div class="topstate">
            <span style='color:#5bd371;' class="iconfont icon-xiadanchenggong"></span>
                <br>
                <label style="font-size:30px;color:#06c4ff;">下单成功</label>
                <br/>
        </div>
        <div class="title">
            <div></div>
            <b>订单详情</b>
        </div>
        <div class="info">
            <div style="float:left;">
                <p>订单价格</p>
                <p>付款方式</p>
                <p>付款状态</p>
                <p>订单编号</p>
                <p>联系人</p>
                <p>联系方式</p>
                <p>收送地址</p>
            </div>
            <div style="float:left;color:#111;">
                <div style="color:red;font-size:25px;margin-top:13px;">￥88</div>
                <p>微信付款</p>
                <p style="color:#00c6ff;">已付款{{result}}</p>
                <p>123456</p>
                <p>张三</p>
                <p>15386659895</p>
                <p>温州职业技术学院</p>
            </div>

        </div>
    </div>
</body>
<script src="vue.min.js"></script>
<script src="./axios.min.js"></script>
<script src="weixinpay.js"></script>
<script>
var app=new Vue({
    el:'#paypage',
    data:{
        val:"xx",
        result:""
    },
    created(){
        var data={
            "appId":"${payResponse.appId}",     //公众号名称，由商户传入
            "timeStamp":"${payResponse.timeStamp}",         //时间戳，自1970年以来的秒数
            "nonceStr":"${payResponse.nonceStr}", //随机串
            "package":"${payResponse.packAge}",
            "signType":"MD5",         //微信签名方式：
            "paySign":"${payResponse.paySign}" //微信签名
        }
        var result=wxpay(data); //result接受结果：cancel，ok，fail
        this.result=result;
    }
});
</script>
<style type="text/css">
    body {
        margin: 0px;
        font-family: "Avenir", Helvetica, Arial, sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        color: #2c3e50;
    }
    .topstate {
        text-align: center;
        padding-top: 50px;
        width: 100%;
        height: 240px;
    }

    .topstate span {
        font-size: 150px;
        margin-bottom: -5px;
    }

    .title {
        display: flex;
        justify-content: flex-start;
    }

    .title div {
        height: 16px;
        width: 7px;
        background: #06c4ff;
        margin: 0px 13px;
        float: left;
    }

    .info {
        line-height: 13px;
        text-indent: 32px;
        font-size: 13px;
        color: #636363;
        display: flex;
        justify-content: flex-start;
    }
</style>
</html>