package com.example.shop.utils;

import com.example.shop.exception.ShopException;
import com.example.shop.exception.ShopSessionException;

import javax.servlet.http.HttpSession;

/**
 * Created by cx on 17-12-27.
 */
public class HttpSessionUtil {

    public static String getOpenid(HttpSession httpSession) {
        try {
            String openid = (String) httpSession.getAttribute("openId");
            return openid;
        } catch (Exception e) {
            throw new ShopSessionException();
        }
    }

}
