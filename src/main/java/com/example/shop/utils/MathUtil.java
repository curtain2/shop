package com.example.shop.utils;

public class MathUtil {

    private static final Double MONEY_RANGE= 0.01;

    public static Boolean equals(double d1,double d2){

        Double r =Math.abs(d1-d2);
        if(r<MONEY_RANGE) {
            return true;
        }
        return false;
    }

}
