package com.example.shop.dto;

import lombok.Data;

/**
 * Created by cx on 17-12-4.
 */
@Data
public class CartDTO {
    private String productId;
    private Integer count;

    public CartDTO(String productId, Integer count) {
        this.productId = productId;
        this.count = count;
    }
}
