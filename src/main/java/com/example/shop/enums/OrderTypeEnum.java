package com.example.shop.enums;

import lombok.Getter;

@Getter
public enum OrderTypeEnum {

    PRACTICALITY_TYPE(1,"实体类订单"),
    SERVICE_TYPE(2,"服务类订单"),
    /**
     * 表示既有实体类又有服务类的产品
     */
    SYNTHESIZE_TYPE(3,"综合类订单")
    ;
    private Integer code;
    private String msg;

    OrderTypeEnum(Integer code,String msg){
        this.code=code;
        this.msg=msg;
    }
}
