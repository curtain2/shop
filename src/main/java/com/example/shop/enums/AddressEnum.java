package com.example.shop.enums;

import lombok.Getter;

/**
 * 表示地址状态码,暴露给前端API
 */
@Getter

public enum AddressEnum {
    DEFUALT(0,"默认地址"),
    UNDEFUALT(1,"非默认地址");


    private Integer code;
    private String msg;
    AddressEnum(Integer code,String msg){
        this.code=code;
        this.msg=msg;
    }

}