package com.example.shop.enums;

import lombok.Data;
import lombok.Getter;

@Getter
public enum  ProductTypeEnum {

    SERVICE(1,"服务类"),
    PRACTICALITY(2,"实物类");

    private Integer code;

    private String message;

    ProductTypeEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
