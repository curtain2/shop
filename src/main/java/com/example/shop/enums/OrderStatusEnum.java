package com.example.shop.enums;

import lombok.Getter;

/**
 *
 */
@Getter
public enum  OrderStatusEnum{

    /*表示未付款订单*/
    NOT_PAY_STATUS(0, "未付款订单"),
    /*表示新订单*/
    NEW(1, "新订单"),
    /*表示已导出订单*/
    EXPORT(2,"已导出订单"),
    /*表示完结订单*/
    FINISHED(3,"完结"),
    /*表示取消订单*/
    CANCEL(10,"取消订单"),

    /*表示未完结订单*/
    UN_FINISHED(30, "未完结订单"),
    /*表示所有订单*/
    ALL_ORDER(1000, "所有的订单"),

    /*服务类订单，订单中的商品存在服务类商品*/
    SERVICE_ORDER(11, "服务类订单"),

    PRACTICALITY(12,"实物类订单")
    ;
    private Integer code;
    private String message;

    OrderStatusEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }






}
