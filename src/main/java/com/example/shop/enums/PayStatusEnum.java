package com.example.shop.enums;

/**
 * Created by cx on 17-10-24.
 */
public enum PayStatusEnum {
    WAIT(0,"未支付"),
    SUCCESS(1,"已支付");

    private Integer code;
    private String msg;

    public Integer getCode(){
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    PayStatusEnum(Integer code, String msg){
        this.code=code;
        this.msg=msg;
    }
}
