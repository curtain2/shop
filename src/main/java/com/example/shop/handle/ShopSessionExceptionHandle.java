package com.example.shop.handle;

import com.example.shop.exception.ShopSessionException;
import com.example.shop.utils.ResultVOUtil;
import com.example.shop.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
@Slf4j
public class ShopSessionExceptionHandle {

    @ResponseBody
    @ExceptionHandler
    public ResultVO handle(Exception e) {
/*        if (e instanceof ShopSessionException) {
            HttpServletResponse httpServletResponse = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
            httpServletResponse.setStatus(401);
        }*/
        return ResultVOUtil.error(e.getMessage());

    }
}
