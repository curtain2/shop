package com.example.shop.service.impl;

import com.example.shop.repository.RoleRepository;
import com.example.shop.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String user) {
        return roleRepository.findByUser(user);
    }
}
