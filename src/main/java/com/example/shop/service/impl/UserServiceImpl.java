package com.example.shop.service.impl;

import com.example.shop.domain.User;
import com.example.shop.exception.ShopException;
import com.example.shop.repository.UserRepository;
import com.example.shop.service.UserService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by cx on 17-11-30.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User save(User user) {

        User rs= userRepository.save(user) ;

        if (rs==null){
            throw  new ShopException("保存失败！");
        }
        else {
            return rs;
        }
    }



    @Override
    public User findByOpenid(String openid) {
        return userRepository.findByOpenId(openid);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findOne(String id) {

        User user = userRepository.findOne(id);

        if (user == null){
                //throw new ShopException("该用户不存在！");\
            return null;
            }
        else {
            return user;
        }
    }
}
