package com.example.shop.service.impl;

import com.example.shop.domain.OrderItem;
import com.example.shop.domain.Product;
import com.example.shop.dto.CartDTO;
import com.example.shop.enums.ResultExceptionEnum;
import com.example.shop.exception.ShopException;
import com.example.shop.repository.OrderItemRepository;
import com.example.shop.repository.ProductRepository;
import com.example.shop.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Override
    public Product findOne(String productId) {
        Product product = productRepository.findOne(productId);
        if(product==null){
            throw new ShopException("通过Id查询商品失败：商品不存在");
        }
        return product;
    }

    @Override
    public List<Product> findByStatus(Integer status) {

        List<Product> productList = productRepository.findByStatus(status);
        if(productList.size()<1){
            throw new ShopException("通过商品状态查询失败：该状态商品不存在");
        }
        return productList;
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Page<Product> findAll(Pageable pageable) {

        return productRepository.findAll(pageable);
    }

    @Override
    public List<Product> findByType(Integer type) {
        return productRepository.findByType(type);
    }

    @Override
    public void delete(String productId) {
        for (OrderItem orderItem:orderItemRepository.findAll()){
            if (orderItem.getProduct().getId()==productId){
                OrderItem result = new OrderItem();
                BeanUtils.copyProperties(orderItem,result);
                result.setProduct(null);

            }
            orderItemRepository.save(orderItem);
        }
        productRepository.delete(productId);
    }

    @Override
    public void updateStatus(String productId, Integer status) {
        Product product =findOne(productId);
        product.setStatus(status);
        productRepository.save(product);
    }

    @Override
    @Transactional
    public void increaseStock(List<CartDTO> cartDTOList) {
        for (CartDTO cartDTO : cartDTOList) {
            Product product = productRepository.findOne(cartDTO.getProductId());
            if (product == null) {
                throw new ShopException(ResultExceptionEnum.PRODUCT_NOT_EXIST);
            }

            Integer result = product.getStock() + cartDTO.getCount();
            product.setStock(result);
            productRepository.save(product);
        }
    }

    @Override
    @Transactional
    public void decreaseStock(List<CartDTO> cartDTOList) {
        for (CartDTO cartDTO : cartDTOList) {
            Product product = productRepository.findOne(cartDTO.getProductId());
            if (product == null) {
                throw new ShopException(ResultExceptionEnum.PRODUCT_NOT_EXIST);
            }

            Integer result = product.getStock() - cartDTO.getCount();
            if (result < 0) {
                throw new ShopException(ResultExceptionEnum.PRODUCT_STOCK_ERROR);
            }
            product.setStock(result);
            productRepository.save(product);
        }
    }

    @Override
    public Product update(String productId, Product product) {
        Product result = productRepository.findOne(productId);
        BeanUtils.copyProperties(product,result);
        productRepository.save(result);
        return result;
    }

    @Override
    public List<Product> findByIdIn(Set<String> productidset) {
        return this.productRepository.findByIdIn(productidset);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }
}
