package com.example.shop.service;

import com.example.shop.domain.Order;
import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundResponse;

public interface PayService {

    PayResponse create(Order order);

    PayResponse notify(String notifyData);

    RefundResponse refund(Order order);
}
