package com.example.shop.service;

import com.example.shop.domain.Product;
import com.example.shop.dto.CartDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface ProductService {

    /*根据商品id  查询商品*/
    Product findOne(String productId);

    List<Product> findByStatus(Integer status);

    Product save(Product product);

    Page<Product> findAll(Pageable pageable);

    List<Product> findByType(Integer type);

    void delete(String productId);

    void updateStatus(String productId, Integer status);

    /*加库存*/
    void increaseStock(List<CartDTO> cartDTOList);

    /*减库存*/
    void decreaseStock(List<CartDTO> cartDTOList);

    /*修改商品*/
    Product update(String productId, Product product);

    List<Product> findByIdIn(Set<String> productidset);

    List<Product> findAll();
}
