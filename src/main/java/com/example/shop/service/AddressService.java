package com.example.shop.service;

import com.example.shop.domain.Address;

import java.util.List;

/**
 * Created by cx on 17-12-7.
 */
public interface AddressService {

   void changeStatus(String openid,String addressId);

   /*增加地址*/
   void addAddress(String openid,Address address);

   /*删除地址*/
   void delAddress(String openid, String id);

   Address findOne(String addressId);

   void update(String addressId,Address address);

   List<Address> findByOpenid(String openid);

}
