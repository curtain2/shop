package com.example.shop.service;

import com.example.shop.domain.User;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

import java.util.List;

/**
 * Created by cx on 17-11-30.
 */

public interface UserService {
    List<User> findAll();

    User findOne(String id);

    User save(User user);

    User findByOpenid(String openid);
}
