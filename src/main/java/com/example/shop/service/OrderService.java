package com.example.shop.service;

import com.example.shop.domain.Order;
import com.example.shop.form.OrderForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface OrderService {

    /**
     * 根据订单表单和微信的用户ID来创建订单，订单默认为创建状态
     * @param orderForm 订单表单
     * @param openid  微信的用户ID
     * @return 创建的订单
     */
    Order create(OrderForm orderForm, String openid);

    //取消订单
    Order cancel(String orderId);

    //删除订单
    void delete(String orderId);

    //支付订单
    Order paid(String orderId);

    /**
     * 根据订单的id,将已导出订单状态修改为完结状态
     * @param orderId 订单的id
     * @return 返回已导出的订单
     */
    Order finish(String orderId);

    /*查询所有订单*/
    Page<Order> findAll(Pageable pageable);

    List<Order> findAll();

    /*查询单个订单*/
    Order findOne(String orderId);

    /*根据用户的openid查询*/
    Page<Order> findByOpenid(String openid,Pageable pageable);

    /*根据订单状态查询*/
    Page<Order> findByOrderStatus(Integer orderStatus,Pageable pageable);

    List<Order> findByOrderStatus(Integer orderStatus);

    /*根据商品的类型查询订单*/
    List<Order> findByProductType(Integer ProductType);

    List<Order> finByOrderTime(String startTime, String endTime);

    List<Order> findByProductSeller(String seller);

    List<Order> save(List<Order> orderList);


    /**
     * 每隔一定的时间执行一次：若订单在规定的时间内未支付，则将订单的状态从创建状态修改为失效状态
     */
    void invalidOrders();

    /**
     * 根据订单状态将订单导出到excel表，若订单状态为新订单 --> 已导出订单
     * @param response 将导出的订单响应返回
     * @param orderStatus 订单的状态
     */
    void exportByOrderStatusAndOrderType(HttpServletResponse response, Integer orderStatus,Integer orderType);

    Order findByOrderNumber(String orderNumber);

    /**
     * 根据一定的时间段，统计所有生成的订单的状况。
     * 统计的格式:
     *      总订单,未完结订单，完结订单，作废订单，新订单，已导出订单
     * @param startTime
     * @param endTime
     * @return
     */
    Map<Integer, Long> statisticsOrder(long startTime, long endTime);

    Page<Order> findByOrderType(PageRequest pageRequest, Integer orderType);

    Page<Order> findByOrderTypeAndOrderStatus(Integer orderType, Integer orderStatus, PageRequest pageRequest);
}
