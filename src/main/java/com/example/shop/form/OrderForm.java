package com.example.shop.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by cx on 17-12-4.
 */
@Data
public class OrderForm {

    @NotEmpty(message = "买家姓名")
    private String name;

    @NotEmpty(message = "买家电话")
    private  String phone;

    @NotEmpty(message = "买家地址")
    private String address;


    //上门取货日期
    private Long deliveryDate;

    //备注
    private String remark;

    @NotEmpty(message = "购物车")
    private String items;

}
