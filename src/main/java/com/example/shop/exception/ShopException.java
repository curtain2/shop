package com.example.shop.exception;


import com.example.shop.enums.ResultExceptionEnum;

public class ShopException extends RuntimeException {



    public ShopException(String msg){
        super(msg);
    }

    public ShopException(ResultExceptionEnum resultExceptionEnum){
        super(resultExceptionEnum.getMessage());
    }

    public ShopException(ResultExceptionEnum resultExceptionEnum, String message){
        super(message);
    }
}
