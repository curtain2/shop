package com.example.shop.exception;


public class ShopSessionException extends RuntimeException {

    public ShopSessionException(){
        super();
    }

    public ShopSessionException(String msg){
        super(msg);
    }

}
