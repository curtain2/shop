package com.example.shop.repository;

import com.example.shop.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;

public interface RoleRepository extends JpaRepository<Role,String> {

    Role findByUser(String user);

}
