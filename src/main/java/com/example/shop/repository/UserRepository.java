package com.example.shop.repository;

import com.example.shop.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by cx on 17-11-30.
 */
@Repository
public interface UserRepository extends JpaRepository<User,String> {


    User findByOpenId(String openid);

}
