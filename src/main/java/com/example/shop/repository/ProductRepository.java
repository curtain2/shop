package com.example.shop.repository;

import com.example.shop.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface ProductRepository extends JpaRepository<Product,String> {

    //根据商品状态查询
    List<Product> findByStatus(Integer status);

    //根据商品类别查询
    List<Product> findByType(Integer type);

    Page<Product> findAll(Pageable pageable);

    List<Product> findByIdIn(Set<String> productidset);

    List<Product> findAll();

    //TODO:根据生效日期 返回所有生效商品


}
