package com.example.shop.repository;

import com.example.shop.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by cx on 17-12-7.
 */
public interface AddressRepository extends JpaRepository<Address,String>{

    List<Address> findByOpenid(String openid);

}
