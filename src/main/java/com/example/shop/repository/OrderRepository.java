package com.example.shop.repository;

import com.example.shop.domain.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order,String> {

    /*根据openid 查询订单*/
    Page<Order> findByOpenid(String openid,Pageable pageable);

    /*根据订单状态查询*/
    Page<Order> findByOrderStatus(Integer orderStatus,Pageable pageable);

    List<Order> findByOrderStatusAndOrderType(Integer orderStatus, Integer orderType);

    List<Order> findAll();

    List<Order> findByCreatetimeBetween(Long startTime,Long endTime);

    List<Order> findByOrderStatus(Integer orderStatus);

    /**
     * 根据number查询订单
     * @param orderNumber 指定的number
     * @return 返回指定number所在的订单
     */
    Order findByNumber(String orderNumber);


    /**
     * 根据订单的类型进行分页查询，otherType为综合订单 -- 即属于实体类也属于服务类订单
     * @param orderType 指定的订单状态
     * @param otherType 服务类订单
     * @param pageable 分页要求
     * @return 返回分页查询的结果
     */
    Page<Order> findByOrderTypeOrOrderType(Integer orderType, Integer otherType, Pageable pageable);

    /**
     * 根据订单类型和订单状态进行查询，otherType为综合订单 -- 即属于实体类也属于服务类订单
     * @param orderType 指定的订单状态
     * @param otherType 服务类订单
     * @param orderStatus 订单的状态
     * @param pageable 分页要求
     * @return 返回分页查询的结果
     */
    @Query("select o from Order o where (o.orderType=?1 or o.orderType=?2) and o.orderStatus=?3")
    Page<Order> findByOrderTypeOrOrderTypeAndOrderStatus(Integer orderType, Integer otherType, Integer orderStatus, Pageable
            pageable);
}
