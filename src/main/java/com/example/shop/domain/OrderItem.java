package com.example.shop.domain;

import com.example.shop.bos.Bostype;
import com.example.shop.bos.Entry;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;

@Entity
@Bostype("A03")
@Table(name = "T_ORDERITEM")
@Setter@Getter
public class OrderItem extends Entry {

    @ManyToOne(fetch=FetchType.LAZY)
    private Product product;


    private Integer count;

    @ManyToOne(fetch = FetchType.LAZY)
    @Access(AccessType.PROPERTY)
    @JsonBackReference
    public Order getParent() {
        return (Order)super.getInnerParent();
    }




}
