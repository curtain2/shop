package com.example.shop.domain;


import com.example.shop.bos.BaseEntity;
import com.example.shop.bos.BosSet;
import com.example.shop.bos.Bostype;
import com.example.shop.bos.IBosSet;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Bostype("A02")
@Table(name = "T_ORDER")
@Getter
@Setter
public class Order extends BaseEntity {

    private String number;

    private String name;

    private String phone;

    private String address;

    /**
     * 微信用户ID
     */
    private String openid;

    private Long amount;

    private Long deliveryDate;

    private String remark;

    private Integer orderStatus;

    private Integer payStatus;

    private Integer orderType;

    @OneToMany(cascade
            = CascadeType.ALL, mappedBy = "parent", orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<OrderItem> items = new HashSet<>();

    public IBosSet<OrderItem> getItems() {
        return new BosSet(this.items, this);
    }

    public Order(){
        setNumberTime();
    }

    /**
     * 根据系统时间的毫秒数，去掉第一位数而生成的一个序列，用来给前端标识订单
     */
    private void setNumberTime() {
        long l = System.currentTimeMillis();
        this.number = String.valueOf(l).substring(1);
    }

}
