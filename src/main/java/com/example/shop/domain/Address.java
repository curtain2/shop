package com.example.shop.domain;

import com.example.shop.bos.BaseEntity;
import com.example.shop.bos.Bostype;
import com.example.shop.enums.AddressEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Bostype("A03")
@Table(name = "T_Address")
@Getter
@Setter
public class Address extends BaseEntity {

    /*用户ID*/
    private String openid;

    /*联系电话*/
    private String phone;

    /*收件人*/
    private String consignee;

    /*收货地址*/
    private String address;

    //@Enumerated
    private int status= AddressEnum.UNDEFUALT.ordinal();


}
