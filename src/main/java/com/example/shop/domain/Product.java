package com.example.shop.domain;

import com.example.shop.bos.BaseEntity;
import com.example.shop.bos.Bostype;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Bostype("A01")
@Table(name = "T_PRODUCT")
@Getter
@Setter
public class Product extends BaseEntity {

    /*名称*/
    private String name;

    /*原价  单位分*/
    private Long old_price;

    /*现价  单位分*/
    private Long price;

    /*商品图片*/
    private String logo;

    /*商品详情图*/
    private String image;

    /*商家*/
    private String seller;

    /*商品描述标题*/
    private String heading;

    /*商品细节图片*/
    private String detailImage;

    /*商品描述*/
    private String description;

    /*商品库存*/
    private Integer stock;

    /*商品生效日期*/
    private Long date;

    /*商品排序*/
    private Integer sort;

    /*商品类别*/
    private Integer type;

    /*商品是否上架*/
    private Integer status;


}
