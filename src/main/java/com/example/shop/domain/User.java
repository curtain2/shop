package com.example.shop.domain;


import com.example.shop.bos.BaseEntity;
import com.example.shop.bos.Bostype;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Bostype("A02")
@Table(name = "T_User")
@Getter@Setter
public class User extends BaseEntity {

    private String name;

    private String openId;

    private String sex;

    private String country;

    private String province;

    private String city;

    private String headImgUrl;

    private String phone;
}