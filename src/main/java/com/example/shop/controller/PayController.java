package com.example.shop.controller;


import com.example.shop.domain.Order;
import com.example.shop.service.OrderService;
import com.example.shop.service.PayService;
import com.example.shop.utils.ResultVOUtil;
import com.example.shop.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.ws.rs.POST;


@RequestMapping("/pay")
@RestController
@CrossOrigin(origins = {}, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS})
@Slf4j
public class PayController {

    @Autowired
    private OrderService orderService;


    @Autowired
    private PayService payService;


    public PayController() {
    }



    @GetMapping("/create")
    public ResultVO create2(@RequestParam("orderid") String orderId) {
        Order order = orderService.findOne(orderId);
        return ResultVOUtil.success(payService.create(order));
    }


    @PostMapping("/notify")
    public ModelAndView notify(@RequestBody String notifyData) {

        payService.notify(notifyData);

        //返回微信处理结果
        return new ModelAndView("pay/success");

    }

}
