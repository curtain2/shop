package com.example.shop.controller;

import com.example.shop.service.UserService;
import com.example.shop.utils.ResultVOUtil;
import com.example.shop.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by cx on 17-11-30.
 */

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = {}, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS})
public class UserController {


    @Autowired
    private UserService userService;

    @RequestMapping("/myuserinfo")
    public ResultVO findByOpenid(Principal principal) {
        return ResultVOUtil.success(userService.findByOpenid(principal.getName()));
    }

    @GetMapping("/findall")
    public ResultVO findAll() {
        return ResultVOUtil.success(userService.findAll());
    }

/*
    //获取用户id，管理员后缀B01
    @RequestMapping({"/user", "/me"})
    public ResultVO user(Principal principal) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("name", principal.getName());
        return ResultVOUtil.success(map);
    }

    @PostMapping("/save")
    public ResultVO save(User user) {
        return ResultVOUtil.success(userService.save(user));
    }

    @PostMapping("/update")
    public ResultVO update(User user) {
        return ResultVOUtil.success(userService.save(user));
    }

    @PostMapping("updatauserinfo")
    public User updatauserinfo(@RequestParam("code") String code, @RequestParam("openId") String openId) {
        User u = userService.findByOpenid(openId);
        return u;
    }
*/
}
