package com.example.shop.controller;


import com.example.shop.domain.User;
import com.example.shop.enums.ResultExceptionEnum;
import com.example.shop.exception.ShopException;
import com.example.shop.service.UserService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


/**
 * 原来使用微信的一套
 * */

@Controller
@RequestMapping("/wechat")
@Slf4j
@CrossOrigin(origins = {},methods ={RequestMethod.GET,RequestMethod.POST, RequestMethod.OPTIONS})
public class WeChatController {

    @Autowired
    private UserService userService;

    @Autowired
    private  WxMpService wxMpService;

    @GetMapping(value = "/authorize")
    public String authorize() {
        //1.配置
        //2.调用方法
        String url = "https://www.embracex.com/gcsweixin/shop/wechat/userinfo";

        String returnUrl ="https://www.embracex.com/rwl/";
        //"https://www.embracex.com/gcsweixin/shop/user/hehe";

     //   "https://www.embracex.com/rwl/"

        String redirectUrl = wxMpService.oauth2buildAuthorizationUrl(url, WxConsts.OAUTH2_SCOPE_USER_INFO, returnUrl);

        return "redirect:" + redirectUrl;
    }


    @GetMapping(value = "/userinfo")
    public String userInfo(@RequestParam("code") String code, @RequestParam("state") String returnUrl,HttpSession httpSession) {

        WxMpOAuth2AccessToken wxMpOAuth2AccessToken;

        try {
            wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);


            if(userService.findByOpenid(wxMpOAuth2AccessToken.getOpenId())==null){
                User user =new User();
                WxMpUser wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, "zh_CN");
                BeanUtils.copyProperties(wxMpUser,user);
                user.setName(wxMpUser.getNickname());
                userService.save(user);
            }

            if(!wxMpService.oauth2validateAccessToken(wxMpOAuth2AccessToken)) {
                        wxMpService.oauth2refreshAccessToken(wxMpOAuth2AccessToken.getRefreshToken());
            }

        }catch (WxErrorException e){
            log.error("【微信网页授权】{}",e);
            throw new ShopException(ResultExceptionEnum.WX_MP_ERROR, e.getError().getErrorMsg());
        }

//        String accessToken = wxMpOAuth2AccessToken.getAccessToken();
        String openId = wxMpOAuth2AccessToken.getOpenId();
        httpSession.setAttribute("openid",openId);
//        String url = "https://api.weixin.qq.com/sns/userinfo?access_token="+accessToken+"&openId="+openId+"&lang=zh_CN";

        return "redirect:"+returnUrl;

    }
}
