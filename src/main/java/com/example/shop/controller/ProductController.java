package com.example.shop.controller;


import com.example.shop.converter.ProductFromToProduct;
import com.example.shop.domain.Order;
import com.example.shop.domain.Product;
import com.example.shop.service.ProductService;
import com.example.shop.utils.FileUtil;
import com.example.shop.utils.KeyUtil;
import com.example.shop.utils.ResultVOUtil;
import com.example.shop.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = {}, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS})

public class ProductController {

    @Autowired
    private ProductService productService;

    @Value("${imagepath}")
    String filepath;

    @Value("${imageurl}")
    String returnurl;

//    @PostMapping()
//    public void exportProduct(HttpServletResponse response){
//        List<Product> orderList = productService.findAll();
//        .returnFile(orderList, response);
//        return ResultVOUtil.success();
//    }


    //处理文件上传  返回文件名
    @RequestMapping(value = "/uploadimg", method = RequestMethod.POST)
    public ResultVO uploadImg(@RequestParam("file") MultipartFile file) {

        String fileName = KeyUtil.genUniqueKey() + file.getOriginalFilename();
        String filePath = filepath;
        try {
            FileUtil.uploadFile(file.getBytes(), filePath, fileName);
        } catch (Exception e) {
            ResultVOUtil.error("上传失败");
        }
        //返回json
        String result = returnurl + fileName;
        return ResultVOUtil.success(result);
    }


    /*获取所有商品 */
    @GetMapping("/getlist")
    public ResultVO getList(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                 @RequestParam(value = "size", defaultValue = "10") Integer size) {
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        System.out.println(principal);
        PageRequest pageRequest = new PageRequest(page, size);
        return ResultVOUtil.success(productService.findAll(pageRequest));
    }

    /*根据商品Id 查询商品*/
    @PostMapping("/getproduct")
    public ResultVO getProduct(@RequestParam("productid") String productId) {
        return ResultVOUtil.success(productService.findOne(productId));
    }

    /*添加商品*/
    @PostMapping("/save")
    public ResultVO save(@RequestBody String product) {

        Product rs = ProductFromToProduct.convert(product);
        return ResultVOUtil.success(productService.save(rs));
    }

    /*修改商品*/
    @PostMapping("/update")
    public ResultVO update(@RequestParam("productid") String productId, @RequestBody String product) {

        Product rs = ProductFromToProduct.convert(product);
        return ResultVOUtil.success(productService.update(productId, rs));
    }

    /*删除商品*/
    @PostMapping("/delete")
    public ResultVO delete(@RequestParam("productid") String productId) {
        productService.delete(productId);
        return ResultVOUtil.success();
    }

    @PostMapping("/updatestatus")
    public ResultVO updateStatus(@RequestParam("productid") String productId, @RequestParam("status") Integer status) {
        productService.updateStatus(productId, status);
        return ResultVOUtil.success();
    }

    /*根据商品是否上架  查询商品*/
    @PostMapping("/findbystatus")
    public ResultVO findByStatus(@RequestParam("status") Integer status) {
        return ResultVOUtil.success(productService.findByStatus(status));
    }

    @PostMapping("/findbytype")
    public ResultVO getList(@RequestParam("type") Integer type) {
        return ResultVOUtil.success(productService.findByType(type));
//        SecurityContextHolder.getContext().getAuthentication().getDetails()
    }

}
