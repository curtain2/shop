package com.example.shop.controller;

import com.example.shop.converter.AddressFormToAddress;
import com.example.shop.domain.Address;
import com.example.shop.domain.User;
import com.example.shop.service.AddressService;
import com.example.shop.service.UserService;
import com.example.shop.utils.ResultVOUtil;
import com.example.shop.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cx on 17-12-7.
 */
@RequestMapping("/address")
@CrossOrigin(origins = {},methods ={RequestMethod.GET,RequestMethod.POST, RequestMethod.OPTIONS})
@RestController
public class AddressController {

    @Autowired
    private AddressService addressService;

    @Autowired
    private UserService userService;


    /*添加地址*/
    @PostMapping("/add")
    public ResultVO addAddressItem(Principal principal, String address){
        addressService.addAddress(principal.getName(),AddressFormToAddress.convert(address));
        return ResultVOUtil.success();
    }

    /*删除地址*/
    @PostMapping("/del")
    public ResultVO delAddressItem(Principal principal,@RequestParam("addressid") String addressId){
        addressService.delAddress(principal.getName(),addressId);
        return ResultVOUtil.success();
    }

    /*获取当前用户的所有地址*/
    @PostMapping("/getaddresslist")
    public ResultVO getList(Principal principal){
        return ResultVOUtil.success(addressService.findByOpenid(principal.getName()));
    }

    /*修改默认地址*/
    @PostMapping("/changestatus")
    public ResultVO changeStatus(Principal principal,@RequestParam("addressid") String addressId){
        addressService.changeStatus(principal.getName(),addressId);
        return ResultVOUtil.success();
    }

    /*修改地址*/
    @PostMapping("/update")
    public ResultVO update(@RequestParam("addressid") String addressId, @RequestBody String address){
        addressService.update(addressId,AddressFormToAddress.convert(address));
        return ResultVOUtil.success();
    }
}
