package com.example.shop.bos;

import java.io.Serializable;

/**
 * Created by liutim on 2017/11/24.
 */
public interface ICoreObject{
    String getId();
}
