package com.example.shop.converter;

import com.example.shop.domain.OrderItem;
import com.example.shop.dto.CartDTO;
import com.example.shop.exception.ShopException;
import com.example.shop.form.OrderForm;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class OrderFormToOrderItem {
    public static List<CartDTO> convert(OrderForm orderForm){
        List<CartDTO> cartDTOList = new ArrayList<>();
        Gson gson = new Gson();

        System.out.println(orderForm.getItems());
        try {
            cartDTOList = gson.fromJson(orderForm.getItems(), new TypeToken<List<CartDTO>>() {
            }.getType());
        } catch (Exception e) {
            log.error("对象转化错误  string={}",orderForm.getItems());
            throw new ShopException("CartDTO转化失败");
        }
        return cartDTOList;
    }
}

//gcsweixin
