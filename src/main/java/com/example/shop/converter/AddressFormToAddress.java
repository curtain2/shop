package com.example.shop.converter;

import com.example.shop.domain.Address;
import com.example.shop.domain.Product;
import com.example.shop.exception.ShopException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by cx on 18-1-5.
 */
@Slf4j
public class AddressFormToAddress {

    public static Address convert(String addressFrom) {
        Address address;
        Gson gson = new Gson();
        System.out.println(addressFrom);
        try {
            address = gson.fromJson(addressFrom, new TypeToken<Address>() {
            }.getType());
        } catch (Exception e) {
            log.error("对象转化错误  string={}",addressFrom);
            throw new ShopException("Adress转化失败");
        }
        return address;
    }

}
