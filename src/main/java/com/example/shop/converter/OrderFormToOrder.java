package com.example.shop.converter;

import com.example.shop.domain.Order;
import com.example.shop.form.OrderForm;
import org.springframework.beans.BeanUtils;

/**
 * Created by cx on 17-12-4.
 */
public class OrderFormToOrder {
    public static Order convert(OrderForm orderForm){
        Order order =new Order();
        BeanUtils.copyProperties(orderForm,order);
        return order;



    }
}
