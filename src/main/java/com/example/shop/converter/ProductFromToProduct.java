package com.example.shop.converter;

import com.example.shop.domain.Product;
import com.example.shop.exception.ShopException;
import com.example.shop.form.OrderForm;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProductFromToProduct {
    public static Product convert(String productFrom) {
        Product product;
        Gson gson = new Gson();
        System.out.println(productFrom);
        try {
            product = gson.fromJson(productFrom, new TypeToken<Product>() {
            }.getType());
        } catch (Exception e) {
            log.error("对象转化错误  string={}",productFrom);
            throw new ShopException("Product转化失败");
        }
        return product;
    }
}
