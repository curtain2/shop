package com.example.shop.config;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@ConditionalOnClass(WxPayService.class)
@EnableConfigurationProperties(WeChatAccountConfig.class)
public class WeChatPayConfigs {

    @Autowired
    private WeChatAccountConfig accountConfig;

    @Bean
    public WxPayServiceImpl wxPayService(){
        WxPayServiceImpl wxPayService = new WxPayServiceImpl();
        wxPayService.setConfig(wxPayConfig());
        return wxPayService;
    }

    @Bean
    @ConditionalOnMissingBean
    public WxPayConfig wxPayConfig(){
        WxPayConfig wxPayConfig = new WxPayConfig();
        wxPayConfig.setAppId(this.accountConfig.getAppId());
        wxPayConfig.setMchId(this.accountConfig.getMchId());
        wxPayConfig.setMchKey(this.accountConfig.getMchKey());
        wxPayConfig.setKeyPath(this.accountConfig.getKeyPath());
        wxPayConfig.setNotifyUrl(this.accountConfig.getNotifyUrl());
        return wxPayConfig;
    }

}
