package com.example.shop.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

import static com.example.shop.config.BosBeanSerializerBuilder.configMappingJackson2HttpMessageConverter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Value("${imagepath}")
    String file;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/image/**").addResourceLocations("file:"+file);
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        for(HttpMessageConverter<?> hmc:converters){
            if(hmc instanceof MappingJackson2HttpMessageConverter){
                MappingJackson2HttpMessageConverter mj2HttpMessageConverter=(MappingJackson2HttpMessageConverter)hmc;
                configMappingJackson2HttpMessageConverter(mj2HttpMessageConverter);
            }
        }
    }



}
